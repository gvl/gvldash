packages:
    - name: gvl_cmdline_utilities
      implementation_class: util.packages.CmdlineUtilPackage
      display_name: Commandline Utilities
      description: >
            Install this package to set up the GVL commandline utilities. These include rstudio,
            JupyterHub (a multi-user IPython notebook server), the public html folder, galaxy-fuse.py and it will also provide commandline
            access to Galaxy modules. A new user account named "researcher" will be created, with the same
            password as your ubuntu user, which you can use to access these utilities.
      services:
        - name: jupyterhub
          display_name: JupyterHub
          process_name: jupyterhub
          virtual_path: "/jupyterhub"
          installation_path: "/usr/local/bin/jupyterhub"
          access_instructions:
             username: researcher
             password_hint: "<cluster password>"
          description: >
            JupyterHub can be used to access your personal IPython Notebook. IPython Notebook is a web-based interactive computational environment where you can
            combine code execution, text, mathematics, plots and rich media into a single document.
        - name: rstudio
          display_name: RStudio
          process_name: rstudio
          virtual_path: "/rstudio"
          installation_path: "/etc/rstudio"
          access_instructions:
             username: researcher
             password_hint: "<cluster password>"
          description: >
            R Studio IDE is a powerful and productive user interface for R.
        - name: public_html
          display_name: "Public HTML"
          process_name: nginx
          virtual_path: "/public/researcher/"
          installation_path: "/home/researcher/public_html"
          description: >
            This is a shared web-accessible folder. Any files you place in this directory will be publicly accessible.

    - name: galaxy_cloudman
      implementation_class: util.packages.GalaxyPackage
      display_name: Galaxy/Cloudman
      description: >
        This package can be used to install or configure Galaxy through CloudMan.
      services:
        - name: galaxy
          display_name: Galaxy
          process_name: galaxy.ini
          virtual_path: "/galaxy"
          installation_path: "/mnt/galaxy/galaxy-app"
          access_instructions:
             username: "manual sign up"
             password_hint: "<custom password>"
          description: >
                Galaxy is an open, web-based platform for accessible, reproducible, and transparent
                computational biomedical research.

    - name: lovd
      implementation_class: util.packages.LovdPackage
      display_name: LOVD
      description: >
        This package can be used to install the Leiden Open Variant Database (LOVD).
      services:
        - name: lovd
          display_name: LOVD
          process_name: php-fpm
          virtual_path: "/lovd"
          installation_path: "/mnt/gvl/apps/lovd"
          access_instructions:
             username: "manual sign up"
             password_hint: "<custom password>"
          description: >
                LOVD stands for Leiden Open (source) Variation Database. LOVD's purpose is to provide a flexible,
                freely available tool for Gene-centered collection and display of DNA variations.
                LOVD 3.0 extends this idea to also provide patient-centered data storage and storage
                of NGS data, even of variants outside of genes.

    - name: cpipe
      implementation_class: util.packages.CpipePackage
      display_name: Cpipe
      description: >
        This package can be used to install Cpipe, a variant detection pipeline designed to process
        high-throughput sequencing data, with the purpose of identifying potentially pathogenic mutations.
      services:
        - name: cpipe
          display_name: Cpipe
          process_name:
          virtual_path:
          access_instructions:
             username: "ubuntu"
             password_hint: "<cluster password>"
          installation_path: "/mnt/gvl/apps/cpipe"
          description: >
                Cpipe is a variant detection pipeline designed to process high-throughput sequencing data,
                with the purpose of identifying potentially pathogenic mutations.

    - name: smrt_portal
      implementation_class: util.packages.SMRTAnalysisPackage
      display_name: SMRT Analysis
      description: >
        This package can be used to install PacBio's open source software suite for single molecule, real-time sequencing.
      services:
        - name: smrt_portal
          display_name: "SMRT Portal"
          process_name: "smrtanalysis/current/redist/java/bin/java"
          virtual_path: "/smrtportal"
          installation_path: "/mnt/gvl/apps/smrtanalysis"
          access_instructions:
             username: "manual sign up"
             password_hint: "<custom password>"
          description: >
                SMRT Portal is PacBio's open source software suite for single molecule, real-time sequencing.
